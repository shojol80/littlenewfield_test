<?php 
//this function run the google analytical script for seo data

if (!function_exists('dt_google_analytical_product_script')) {
    function dt_google_analytical_product_script($id, $v){
        $params = array("id" => $id);
        $info = get_content($params);
        if(isset($info)){
            // dd($info);
            $image = '"';
            $sku = "";
            $name = "";
            $url = site_url();
            $des = strip_tags($info[0]['content_body']);
            // $des = substr($des, 0, 10);
            $count = count($info[0]['media']);
            for($i=0;$i<$count;$i++){
                $image .= $info[0]['media'][$i]['filename'];
                if(($i+1)<$count){
                    $image.='",
                    "';
                }
                elseif(($i+1)==$count){
                    $image.='"';
                }
            }
            $brand = null;
            $count = count($info[0]['contentData']);
            for($i=0;$i<$count;$i++){
                if($info[0]['contentData'][$i]['field_name'] == "sku"){
                    if($info[0]['contentData'][$i]['field_value']!=null){
                        $sku = $info[0]['contentData'][$i]['field_value'];
                    }
                }
                if($info[0]['contentData'][$i]['field_name'] == "brand"){
                    if($info[0]['contentData'][$i]['field_value']!=null){
                        $brand = $info[0]['contentData'][$i]['field_value'];
                    }
                }
            }
            $brand_show ="";
            if($brand != null){
                $brand_show = <<<EOD
                "brand": {
                                "@type": "Brand",
                                "name": "{$brand}"
                                },
                EOD;
            }
            if($info[0]['created_by'] != null){
                $user_id = $info[0]['created_by'];
                $name = user_name($user_id, $mode = 'full');
            } else{
                $user_id = (int) get_users("is_admin=1")[0]['id'];
                $name = user_name($user_id, $mode = 'full');
            }
            $offers = "";
            $offer_check = DB::table('offers')->where('product_id', $id)->first();
            if(isset($offer_check)){
                $time = date('Y-m-d', strtotime($offer_check->expires_at));
                $offers = <<<EOD
                "offers": {
                                "@type": "Offer",
                                "url": "{$info[0]['url']}",
                                "price": "{$v}",
                                "priceCurrency": "EUR",
                                "priceValidUntil": "{$time}",
                                "availability": "https://schema.org/InStock"
                                }
                EOD;
            }
            else{
                $time = date('Y-m-d', strtotime('now'. ' +'.'60 days'));
                $offers = <<<EOD
                "offers": {
                                "@type": "Offer",
                                "url": "{$info[0]['url']}",
                                "price": "{$v}",
                                "priceCurrency": "EUR",
                                "priceValidUntil": "{$time}",
                                "availability": "https://schema.org/InStock"
                                }
                EOD;
            }
            // dd($offer_check);
            template_head('<script type="application/ld+json">
            {
                "@context": "https://schema.org/",
                "@type": "Product",
                "name": "'.$info[0]['title'].'",
                "image": [
                    '.$image.'
                ],
                "description": "'.$des.'",
                "sku": "'.$sku.'",
                "author": {
                "@type": "Person",
                "name": "'.$name.'"
                },
                "datePublished": "'.date('Y-m-d',strtotime($info[0]['created_at'])).'",
                '.$brand_show.'
                '.$offers.'
            }
            </script>');
        }
    }
}

function basicGoogleAnalytical(){
    $ads_id  = get_option('google-ads-id', 'website');
    $tags_id  = get_option('google-tag-manager-id', 'website');
    if($ads_id){
        template_head("<script async src='https://www.googletagmanager.com/gtag/js?id=AW-".$ads_id."'></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'AW-".$ads_id."');
        </script>");
    }
    if($tags_id){
        template_head("<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id=".$tags_id."'+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','".$tags_id."');</script>");
    }


}

function googleAnalyticalOrder($price, $currency, $id){
    $ads_label  = get_option('google-ads-label', 'website');
    $ads_id  = get_option('google-ads-id', 'website');
	if($ads_label && $ads_id){
		template_head("<script>
		  gtag('event', 'conversion', {
		      'send_to': 'AW-".$ads_id."/".$ads_label."',
		      'value': ".$price.",
		      'currency': '".$currency."',
		      'transaction_id': '".$id."'
		  });
		</script>");
	}
}