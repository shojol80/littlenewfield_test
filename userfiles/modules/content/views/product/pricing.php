<div class="card style-1 mb-3">
    <div class="card-header no-border">
        <h6><strong>Pricing</strong></h6>
    </div>

    <div class="card-body pt-3">
        <div class="row">

            <div class="col-md-12">
                <label>Price</label>
                <div class="input-group mb-3 prepend-transparent">
                    <div class="input-group-prepend">
                        <span class="input-group-text text-muted"><?php echo get_currency_code(); ?></span>
                    </div>
                    <input type="text" class="form-control js-product-price" required name="price" value="<?php echo $productPrice; ?>">
                </div>
            </div>

            <?php
            if (is_module('shop/offers')):
            ?>
                <module type="shop/offers/special_price_field" product_id="<?php echo $product['id'];?>" />
            <?php endif; ?>

        </div>
    </div>
</div>



<?php
   use Illuminate\Support\Facades\DB;
   
?>

<?php if($product['id']): ?>
    <?php
        $productUpsellingData = DB::table('product_upselling')->get()->all();
        $selectproductUpsellingData = DB::table('product_upselling_item')->where('product_id',$product['id'])->get(['item_id']);
        $itemcount = 0; 
        $passloop = false;
    ?>

    <div class="card style-1 mb-3">
        <div class="card-header no-border">
        <h6><strong>Product Upselling</strong></h6>
        </div>
        <div class="card-body pt-3">
            <div class="row">
                <?php if(isset($productUpsellingData)):  ?>
                    <?php foreach($productUpsellingData as $pitem):  ?>
                        <?php if(!$selectproductUpsellingData->count()): ?>
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label><input type="checkbox" id="<?php print $pitem->id;  ?>" value="<?php print $pitem->id;  ?>"> <?php print $pitem->serviceName;  ?></label>
                                </div>
                            </div>
                        <?php else: ?>
                            <?php foreach($selectproductUpsellingData as $sitem):  ?>
                                <?php if($pitem->id == $sitem->item_id): ?>
                                    <div class="col-md-6">
                                        <div class="checkbox">
                                            <label><input type="checkbox" checked id="<?php print $pitem->id;  ?>" value="<?php print $pitem->id;  ?>"> <?php print $pitem->serviceName;  ?></label>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <?php
                                        $itemcount++;
                                        if($selectproductUpsellingData->count() == $itemcount){
                                            $passloop = true;
                                        }
                                    ?>
                                <?php endif; ?>
                            <?php endforeach;  ?>
                        <?php endif; ?>

                        <?php if($passloop): 
                        $passloop = false; 
                        $itemcount = 0;
                        ?>
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label><input type="checkbox" id="<?php print $pitem->id;  ?>" value="<?php print $pitem->id;  ?>"> <?php print $pitem->serviceName;  ?></label>
                                </div>
                            </div>
                        <?php else: ?>
                                <?php
                                    $passloop = false; 
                                    $itemcount = 0;
                                ?>
                        <?php endif; ?>
                        <script>
                            $('#<?php print $pitem->id;  ?>').click(function() {
                                if($('#<?php print $pitem->id;  ?>').is(':checked')){
                                    var product_id = '<?php echo $product['id'];?>';
                                    var item_id = $('#<?php print $pitem->id;  ?>').val();
                                    $.ajax({
                                        type: "POST",
                                        url: "<?=api_url('productUpsellingItemInsert')?>",
                                        data:{ product_id : product_id, item_id : item_id },
                                        success: function(response) {
                                            console.log(response.message);  
                                        },
                                        error: function(response){
                                            console.log(response.responseJSON.message);
                                        }
                                    });
                                
                                }
                                else{
                                    var product_id = '<?php echo $product['id'];?>';
                                    var item_id = '<?php print $pitem->id;  ?>';
                                    $.ajax({
                                        type: "POST",
                                        url: "<?=api_url('productUpsellingItemDelete')?>",
                                        data:{ product_id : product_id, item_id : item_id },
                                        success: function(response) {
                                            console.log(response.message);  
                                        },
                                        error: function(response){
                                            console.log(response.responseJSON.message);
                                        }
                                    });
                                
                                }
                    
                                });

                            
                        </script>
                    <?php endforeach;  ?>
                <?php endif;  ?>
            </div>
        </div>
    </div>
<?php endif; ?>



<?php if($product['id']): ?>
    <div class="card style-1 mb-3">
        <div class="card-header no-border">
        <h6><strong>Thank You Product Templates</strong></h6>
        </div>
        <div class="card-body pt-3">
            <div class="row">
                <?php
                    for ($x = 1; $x <= 6; $x++) {
                ?> 
                    <?php if(DB::table("thank_you_pages")->where('template_name',$x)->where('product_id',$product['id'])->get()->count()):  ?>
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label><input onclick="check('<?php print $x; ?>')" checked type="checkbox" id="theme<?php print $x; ?>" value="<?php print $x; ?>"> Thank You-<?php print $x; ?> </label>
                        </div>
                    </div>
                    <?php else:  ?>
                        <div class="col-md-4">
                            <div class="checkbox">
                            <label><input onclick="check('<?php print $x; ?>')"  type="checkbox" id="theme<?php print $x; ?>" value="<?php print $x; ?>"> Thank You-<?php print $x; ?> </label>
                            </div>
                        </div>
                    <?php endif; ?>     
                <?php
                    }
                ?> 
            </div>
        </div>
    </div>

    <script>
     function check(id){
        if($('#theme'+id).is(':checked')){

            templateName = $('#theme'+id).val(); 
            console.log(templateName);
            $.ajax({
                type: "POST",
                url: "<?=api_url('productModulesInsert')?>",
                data:{ template_name : templateName, product_id : '<?php echo $product['id'];?>' },
                success: function(response) {
                    console.log(response.message);  
                },
                error: function(response){
                    console.log(response.responseJSON.message);
                }
            });
        }
        else{
            templateName = $('#theme'+id).val(); 
            console.log(templateName);
            $.ajax({
                type: "POST",
                url: "<?=api_url('productModulesDelete')?>",
                data:{ template_name : templateName, product_id : '<?php echo $product['id'];?>' },
                success: function(response) {
                    console.log(response.message);  
                },
                error: function(response){
                    console.log(response.responseJSON.message);
                }
            });
        }
     }
    
    </script>
<?php endif; ?>






<?php if($product['id']): ?>
    <div class="card style-1 mb-3">
        <div class="card-header no-border">
        <h6><strong>Checkout Bumbs </strong></h6>
        </div>
        <div class="card-body pt-3">
            <div class="row">
                    <?php if(DB::table("checkout_bumbs")->where('product_id',$product['id'])->get()->count()):  ?>
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label><input onclick="bumbs()" checked type="checkbox" id="checkBumbs" > Select This Product </label>
                        </div>
                    </div>
                    <?php else:  ?>
                        <div class="col-md-4">
                            <div class="checkbox">
                            <label><input onclick="bumbs()"  type="checkbox" id="checkBumbs"> Select This Product </label>
                            </div>
                        </div>
                    <?php endif; ?>     
            </div>
        </div>
        <div class="card-body pt-3" style="margin-left: 10px; ">
            <div class="row">
                <?php if(DB::table("checkout_bumbs")->where('show_cart',1)->get()->count()):  ?>
                    <div class="form-check" style="margin-right: 10px; ">
                        <input class="form-check-input" type="radio" onclick="checkbumbs('scart',1,0)" name="flexRadioDefault" id="scart" checked> 
                        <label class="form-check-label" for="flexRadioDefault1">
                            Shopping Cart Bumbs
                        </label>
                    </div>
                <?php else: ?>
                    <div class="form-check" style="margin-right: 10px; ">
                        <input class="form-check-input" onclick="checkbumbs('scart',1,0)" type="radio" name="flexRadioDefault" id="scart">
                        <label class="form-check-label" for="flexRadioDefault2">
                            Shopping Cart Bumbs
                        </label>
                    </div>
                <?php endif; ?>  
                <?php if(DB::table("checkout_bumbs")->where('show_checkout',1)->get()->count()):  ?>
                    <div class="form-check" style="margin-right: 10px; ">
                        <input class="form-check-input" type="radio" onclick="checkbumbs('scheckout',0,1)" name="flexRadioDefault" id="scheckout" checked>
                        <label class="form-check-label" for="flexRadioDefault1">
                            Checkout Page-2 Bumbs
                        </label>
                    </div>
                <?php else: ?>
                    <div class="form-check" style="margin-right: 10px; ">
                        <input class="form-check-input" onclick="checkbumbs('scheckout',0,1)" type="radio" name="flexRadioDefault" id="scheckout">
                        <label class="form-check-label" for="flexRadioDefault2">
                            Checkout Page-2 Bumbs
                        </label>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <script>
     function bumbs(id){
        if($('#checkBumbs').is(':checked')){
            if($('#scart').is(':checked')){
                show_cart = 1;
            }else{
                show_cart = 0;
            }
            if($('#scheckout').is(':checked')){
                show_checkout = 1;
            }else{
                show_checkout = 0;
            }
            $.ajax({
                type: "POST",
                url: "<?=api_url('checkoutBumbsInsert')?>",
                data:{ product_id : '<?php echo $product['id'];?>',show_cart : show_cart, show_checkout: show_checkout },
                success: function(response) {
                    console.log(response.message);  
                },
                error: function(response){
                    console.log(response.responseJSON.message);
                }
            });
        }
        else{
            $.ajax({
                type: "POST",
                url: "<?=api_url('checkoutBumbsDelete')?>",
                data:{ product_id : '<?php echo $product['id'];?>' },
                success: function(response) {
                    $('#scart').prop('checked', false);
                    $('#scheckout').prop('checked', false);
                    console.log(response.message);  
                },
                error: function(response){
                    console.log(response.responseJSON.message);
                }
            });
        }
     }


     function checkbumbs(name,show_cart,show_checkout){
        if($('#'+name).is(':checked')){
            $.ajax({
                type: "POST",
                url: "<?=api_url('activeBumbs')?>",
                data:{ show_cart : show_cart, show_checkout: show_checkout },
                success: function(response) {
                    $('#'+name).prop('checked', true);
                    console.log(response.message);  
                },
                error: function(response){
                    console.log(response.responseJSON.message);
                }
            });
        }       
     }
    
    
    </script>
<?php endif; ?>


