<div class="row copyright mt-3">
    <div class="col-12">
        <p class="text-muted text-center small"><?php _e('Open-source website builder and CMS Droptienda.Shopsystem & Template by Droptienda®'); ?></p>
    </div>
    <div class="col-12">
        <p class="text-muted text-center small">Droptienda® Version <?php print(Config::get('app.adminTemplateVersion')); ?></p>
    </div>
</div>