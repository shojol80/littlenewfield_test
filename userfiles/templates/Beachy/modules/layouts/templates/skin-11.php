<div class="container safe-mode nodrop">
    <div class="row">
        <div class="col-xl-12 mx-auto">
            <div class="row justify-content-between">

                    <div class="col-sm-12 col-md-4 col-lg-3 col-xl-3">
                        <?php include TEMPLATE_DIR . 'layouts' . DS . "shop_sidebar.php" ?>
                    </div>

                <div class="col-sm-12 col-md-8 col-lg-9 col-xl-9">
                    <module type="shop/products" />
                </div>
            </div>
        </div>
    </div>
</div>