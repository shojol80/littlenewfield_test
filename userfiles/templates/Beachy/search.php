<?php

/*

type: layout
content_type: dynamic
name: Search

*/


?>

<?php include template_dir() . "header.php"; ?>

<?php
$keywords = '';
if (isset($_GET['keywords'])) {
    $keywords = htmlspecialchars($_GET['keywords']);
}

$searchType = '';
if (isset($_GET['search-type'])) {
    $searchType = htmlspecialchars($_GET['search-type']);
}
?>
<?php if ($searchType == 'blog' OR $searchType == ''): ?>
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="m-auto allow-drop" style="max-width: 800px;">
                        <h1 class="hr edit" field="search_header" rel="content">Results found<span class="text-primary">.</span></h1>
                        <p class="lead"><em>Erwähnung</em> &ldquo;<?php print $keywords; ?>&rdquo;</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<section>
    <div class="container">
        <?php if ($searchType == 'blog' OR $searchType == ''): ?>
            <?php if(Config::get('custom.search_hits')==1): ?>
                <div class="shop-search-wrapper">
                    <h2 style="margin-bottom:20px">Products</h2>
                    <module type="shop/products" limit="18" keyword="<?php print $keywords; ?>" description-length="70"/>
                </div>
                <div class="shop-search-wrapper">
                    <h2 style="margin-bottom:20px">Blog</h2>
                    <module type="posts" limit="18" keyword="<?php print $keywords; ?>" description-length="70"/>
                </div>
            <?php else: ?>
                <div class="shop-search-wrapper">
                    <h2 style="margin-bottom:20px">Blog</h2>
                    <module type="posts" limit="18" keyword="<?php print $keywords; ?>" description-length="70"/>
                </div>
                <div class="shop-search-wrapper">
                    <h2 style="margin-bottom:20px">Products</h2>
                    <module type="shop/products" limit="18" keyword="<?php print $keywords; ?>" description-length="70"/>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</section>


<?php include template_dir() . "footer.php"; ?>
